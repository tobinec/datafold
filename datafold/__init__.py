#!/usr/bin/env python3

import datafold.appfold
import datafold.dynfold
import datafold.pcfold
from datafold._version import Version, show_versions

# __version__ is attribute by convention
__version__ = Version.v_short
